#!/usr/bin/env ruby
# frozen_string_literal: true
#
# Copyright (C) 2016 Harald Sitter <sitter@kde.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) version 3, or any
# later version accepted by the membership of KDE e.V. (or its
# successor approved by the membership of KDE e.V.), which shall
# act as a proxy defined in Section 6 of version 3 of the license.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.  If not, see <http://www.gnu.org/licenses/>.

require 'fileutils'
require 'git'

def list
  Dir.glob('*', File::FNM_DOTMATCH).reject { |x| %w(. ..).include?(x) }
end

ENV['GITSLAVE'] = '.gitslave'
FileUtils.rm_r(Dir.glob('*') - %w(debian), verbose: true)
repo = Git.clone('https://invent.kde.org/graphics/digikam.git', 'digikam')
system('./download-repos', chdir: repo.dir.path) || raise
FileUtils.rm_r(Dir.glob("#{repo.dir.path}/**/.git"), verbose: true)
FileUtils.cp_r("#{repo.dir.path}/.", Dir.pwd, verbose: true)
FileUtils.rm_r(repo.dir.path)

dirname = File.basename(Dir.pwd)
Dir.chdir('..') do
  tar = Dir.glob('*.orig.tar.xz')
  tar.size == 1 || raise
  tar = tar[0]
  FileUtils.rm(tar, verbose: true)
  system('tar', "--exclude=#{dirname}/debian", '-cJf', tar, dirname) || raise
end
